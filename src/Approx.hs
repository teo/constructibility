{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE BangPatterns #-}
module Approx where
import Data.Real.Constructible
import Data.Number.IReal
import Data.Number.IReal.IReal (appr)
import Data.Number.IReal.IntegerInterval (lowerI, upperI)
import Control.DeepSeq (NFData(..))

type R = Approx
data Approx = Approx IReal Construct

instance Show Approx where
  show (Approx a _) = show a

precision, precisionBin :: Int
precision = 14
precisionBin = dec2bits precision

instance Eq Approx where
  {-# INLINE (==) #-}
  (Approx x1 y1) == (Approx x2 y2) = (x1 =?= x2 `atDecimals` precision) && y1 == y2

instance Ord Approx where
  {-# INLINE (<) #-}
  (Approx x1 y1) < (Approx x2 y2) = (x1 <! x2 `atDecimals` precision) || y1 < y2
  {-# INLINE (>) #-}
  (Approx x1 y1) > (Approx x2 y2) = (x1 >! x2 `atDecimals` precision) || y1 > y2
  {-# INLINE compare #-}
  compare (Approx x1 y1) (Approx x2 y2) =
    compareTo x1 x2 <> compare y1 y2

instance NFData Approx where
  rnf (Approx !_ !_) = ()

compareTo :: IReal -> IReal -> Ordering
{-# INLINE compareTo #-}
compareTo x y
  | upperI ax <= lowerI ay = LT
  | lowerI ax >= upperI ay = GT
  | otherwise = EQ
  where
    ax = appr x precisionBin
    ay = appr y precisionBin

liftApprox :: (IReal -> IReal -> IReal) -> (Construct -> Construct -> Construct) -> Approx -> Approx -> Approx
{-# INLINE liftApprox #-}
liftApprox f g (Approx a1 b1) (Approx a2 b2) = Approx (f a1 a2) (g b1 b2)

mapApprox :: (IReal -> IReal) -> (Construct -> Construct) -> Approx -> Approx
{-# INLINE mapApprox #-}
mapApprox f g (Approx a b) = Approx (f a) (g b)

instance Num Approx where
  {-# INLINE (+) #-}
  (+) = liftApprox (+) (+)
  {-# INLINE (-) #-}
  (-) = liftApprox (-) (-)
  {-# INLINE (*) #-}
  (*) = liftApprox (*) (*)
  {-# INLINE negate #-}
  negate = mapApprox negate negate
  {-# INLINE signum #-}
  signum = mapApprox signum signum
  {-# INLINE abs #-}
  abs = mapApprox abs abs
  {-# INLINE fromInteger #-}
  fromInteger x = Approx (fromInteger x) (fromInteger x)

instance Fractional Approx where
  {-# INLINE (/) #-}
  (/) = liftApprox (/) (/)
  {-# INLINE recip #-}
  recip = mapApprox recip recip
  {-# INLINE fromRational #-}
  fromRational x = Approx (fromRational x) (fromRational x)

instance Floating Approx where
  {-# INLINE sqrt #-}
  sqrt x | x == 0 = 0
         | otherwise = mapApprox sqrt sqrt x

instance Powers Approx where
  {-# INLINE sq #-}
  sq = mapApprox sq sq
  {-# INLINE pow #-}
  pow x i = mapApprox (flip pow i) (flip pow i) x

instance Powers Construct where
  {-# INLINE sq #-}
  sq x = x * x

constructToApprox :: Construct -> Approx
constructToApprox x = Approx (fromC x) x

-- | use exact part to recalculate approximation.
reApprox :: Approx -> Approx
reApprox (Approx _ x) = Approx (fromC x) x

fromC :: Construct -> IReal
fromC = fromC' . deconstruct

fromC' :: Either Rational (Construct, Construct, Construct) -> IReal
fromC' (Left x) = fromRational x
fromC' (Right (a, b, c)) = fromC a + (fromC b * sqrt (fromC c))
