#!/usr/bin/env bash
START=0
INT=0.0001
for i in 0 1 2 3 4 5 6 7 8 9 10 
do
	stack build --force-dirty --ghc-options -DLEFT=0$(bc <<< "$START + $i*$INT") --ghc-options -DRIGHT=0$(bc <<< "$START + (1 + $i)*$INT") && time stack run >> out.txt
done

