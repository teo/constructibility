{-# LANGUAGE RecordWildCards #-}
import Test.Tasty
import Test.Tasty.Ingredients.Basic (testsNames)
import Test.Tasty.HUnit hiding (assert)
import Test.Tasty.Golden (goldenVsFile, writeBinaryFile)
import Test.Tasty.Hedgehog
import Data.Real.Constructible (Construct)
import Hedgehog
import Control.Monad
import Control.Applicative
import Debug.Trace
import Data.Number.IReal

import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Approx
import Geometry

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [constructibilitySequence, hedgehogTests]

golden :: Show a => TestName -> IO a -> TestTree
golden name act =
  goldenVsFile name goldPath outPath $ do
    out <- act
    writeBinaryFile outPath (show out)
  where
    goldPath = ".golden/" ++ name ++ ".gold"
    outPath = ".golden/" ++ name ++ ".out"

constructibilitySequence :: TestTree
constructibilitySequence = testGroup "First few constructiblity numbers"
  [ testCase "first (length)" $
      length (constructibilityNumber 0) @?= 2
  , golden "first (golden)" $
      pure $ constructibilityNumber 0
  , testCase "second (length)" $
      length (constructibilityNumber 1) @?= 6
  , golden "second (golden)" $
      pure $ constructibilityNumber 1
  , testCase "third (length)" $
      length (constructibilityNumber 2) @?= 203
  , golden "third (golden)" $
      pure $ constructibilityNumber 2
  -- rest take too long to compute
  ]


hedgehogTests :: TestTree
hedgehogTests = testGroup "Property tests"
  [ testProperty "collision point of two lines lies on both" prop_linecollision
  , testProperty "collision point of two circles lies on both" prop_circlecollision
  , testProperty "distinct circles with same centre don't collide" prop_circle_samecentre
  , testProperty "collision point of line and circle lies on both" prop_linecirclecollision
  ]

genConstruct :: MonadGen m => Range Int -> m Construct
genConstruct rng =
  Gen.recursive Gen.choice
    [fromIntegral <$> Gen.integral rng]
    [Gen.subterm3 genC genC genC (\a b c -> a + b*sqrt (max 1 c))]
  where
    genC = genConstruct rng

genR :: MonadGen m => m R
genR = constructToApprox <$> Gen.resize 4 (genConstruct (Range.constant (-10) 10))

genPoint :: MonadGen m => m Point
genPoint = Point <$> genR <*> genR

genSlope :: MonadGen m => m Slope
genSlope = Gen.frequency [(10, pure Inf), (90, Fin <$> genR)]

genLine :: MonadGen m => m Line
genLine = Line <$> genSlope <*> genR

genCircle :: MonadGen m => m Circle
genCircle = do
  a <- genR
  b <- genR
  r <- abs <$> genR
  let rsq = sq r
  pure Circle{..}

prop_linecollision :: Property
prop_linecollision =
  property $ do
     l1 <- forAll genLine
     l2 <- forAll genLine
     when (l1 == l2) discard
     let intersections = intersectLL l1 l2
     mapM_ (\(p,l) -> assert $ pointOnLine p l) (liftA2 (,) intersections [l1,l2])

prop_circlecollision :: Property
prop_circlecollision =
  property $ do
     c1 <- forAll genCircle
     c2 <- forAll genCircle
     when (c1 == c2) discard
     let intersections = intersectCC c1 c2
     mapM_ (\(p,c) -> assert $ pointOnCircle p c) (liftA2 (,) intersections [c1,c2])

prop_circle_samecentre :: Property
prop_circle_samecentre =
  property $ do
    centre <- forAll genPoint
    r1 <- forAll genPoint
    r2 <- forAll genPoint
    let c1 = circle centre r1
    let c2 = circle centre r2
    when (c1 == c2) discard
    assert $ null (intersectCC c1 c2)


prop_linecirclecollision :: Property
prop_linecirclecollision =
  property $ do
     c1 <- forAll genCircle
     l2 <- forAll genLine
     let intersections = intersectCL c1 l2
     mapM_ (\p -> assert $ pointOnCircle p c1) intersections
     mapM_ (\p -> assert $ pointOnLine p l2) intersections
